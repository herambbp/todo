const taskElement           =       document.querySelector('#task');
const taskForm              =       document.querySelector('#taskForm');
const editTaskForm          =       document.querySelector('#editTaskForm');
const taskList              =       document.querySelector('#taskList');
const clearAllTasksBtn      =       document.querySelector('#clearAllTasksBtn');
const emptyModal            =       document.querySelector('#emptyModal');
const nameModal             =       document.querySelector('#nameModal');
const nameInputSubmit       =       document.querySelector('#nameInputBtn');
const inputUserName         =       document.querySelector('#userName');
const removeModal           =       document.querySelector('#removeModal');
const removeTask            =       document.querySelector('#removeTask');
const editTaskSubmit        =       document.querySelector('#editTaskSubmit');
const editModal             =       document.querySelector('#editModal');
const taskSearchElement     =       document.querySelector('#taskSearch');
const TASKS_KEY             =       'tasks';
const dueDatePicker         =       document.querySelector('#dueDate');
const prioritySelect        =       document.querySelector('#prioritySelect');
const editPrioritySelect    =       document.querySelector('#editPrioritySelect');
const editDueDatePicker     =        document.querySelector('#editDueDate');
const priorityFilter        =       document.querySelector('#priorityFilter');
const dueDateFilter         =       document.querySelector('#dueDateFilter');
const editTask              =       document.querySelector('#editTask')
const tasks                 =       taskList.children;
let filteredTasksList       =       getTasksFromLocalStorage();

let 
emptyModalObj, removeModalObj, nameModalObj;


// Entry Point
initialize();
function initialize() {
    taskForm.addEventListener('submit', handleSubmit);
    taskList.addEventListener('click', handleConfirm);
    removeTask.addEventListener('click', handleRemoveTask);
    document.addEventListener('DOMContentLoaded', initializeModals);
    document.addEventListener('DOMContentLoaded', handleGreeting);
    clearAllTasksBtn.addEventListener('click', handleClearAll);
    document.addEventListener('DOMContentLoaded', loadTasks);
    taskSearchElement.addEventListener('keyup', handleSearch);
    priorityFilter.addEventListener('change', handlePriorityFilters);
    dueDateFilter.addEventListener('change', handleDueDateFilters);
    priorityFilter.addEventListener('change', handlePriorityFilters);
    dueDateFilter.addEventListener('change', handleDueDateFilters);
    editTaskSubmit.addEventListener('click', handleEditSubmit);
    taskList.addEventListener('click', handleEditForm);
    nameInputSubmit.addEventListener('click',getName);
}



// Helper Functions
function getTasksFromLocalStorage() {
    let tasks = localStorage.getItem(TASKS_KEY);
    if(!tasks) {
        return [];
    }
    return JSON.parse(tasks);
}

function isDeleteElement(e) {
    return e.target.classList.contains('delete-item') || e.target.parentElement.classList.contains('delete-item');
}

function isEditElement(e) {
    return e.target.classList.contains('edit-item') || e.target.parentElement.classList.contains('edit-item');
}

function addTaskElement(taskObj) {
    const span = document.createElement('span');
    span.classList.add('col');
    span.classList.add('s4');
    const innerSpan = document.createElement('span');
    innerSpan.classList.add('col');
    innerSpan.classList.add('s2');

    const list = document.createElement('LI');
    list.classList.add('collection-item');
    list.classList.add('task-item');
    list.classList.add('row');
    list.classList.add('lighten-4');
    list.classList.add('grey');
    list.classList.add('black-text');
    list.setAttribute('data-id', taskObj.id);
    span.appendChild(document.createTextNode(`${taskObj.task}`));
    list.appendChild(span);
    
    const removeLink = document.createElement('A');
    removeLink.classList.add('delete-item');
    removeLink.classList.add('ms4');
    removeLink.classList.add('secondary-content');
    removeLink.setAttribute('href', '#');
    removeLink.setAttribute('title', 'Remove Task');
    
    const removeIcon = document.createElement('I');
    removeIcon.classList.add('fa');
    removeIcon.classList.add('fa-trash');
    removeIcon.classList.add('red-text');

    removeLink.appendChild(removeIcon);
    innerSpan.appendChild(removeLink);

    const editLink = document.createElement('A');
    editLink.classList.add('edit-item');
    editLink.classList.add('secondary-content');
    editLink.classList.add('ms2');
    editLink.classList.add('yellow-text');
    editLink.classList.add('text-darken-3');
    editLink.setAttribute('href', '#');
    editLink.setAttribute('title', 'Edit Task');
    const editIcon = document.createElement('I');
    editIcon.classList.add('fa');
    editIcon.classList.add('fa-edit');

    editLink.appendChild(editIcon);
    innerSpan.appendChild(editLink);

    list.appendChild(addBadges(taskObj));
    list.appendChild(innerSpan);
    taskList.appendChild(list);
}

function addBadges(taskObj) {

    if(taskObj.priority === 'High') listBgColor = 'red';
    else if(taskObj.priority === 'Med') listBgColor = 'yellow';
    else listBgColor = 'green';

    const secondInnerSpan = document.createElement('span');
    secondInnerSpan.classList.add('col');
    secondInnerSpan.classList.add('s6');

    const priorityBadge = document.createElement('SPAN');
    priorityBadge.classList.add(listBgColor);
    priorityBadge.classList.add('secondary-content');
    priorityBadge.classList.add('badge');
    priorityBadge.classList.add('round');
    priorityBadge.classList.add('ms2');
    priorityBadge.classList.add('darken-4');
    priorityBadge.classList.add('white-text');
    priorityBadge.appendChild(document.createTextNode(taskObj.priority));
    secondInnerSpan.appendChild(priorityBadge);
    
    const dueDateBadge = document.createElement('SPAN');
    dueDateBadge.classList.add('white');
    dueDateBadge.classList.add('secondary-content');
    dueDateBadge.classList.add('badge');
    dueDateBadge.classList.add('round');
    dueDateBadge.classList.add('ms2');
    dueDateBadge.classList.add('black-text');
    dueDateBadge.appendChild(document.createTextNode(taskObj.dueDate));
    secondInnerSpan.appendChild(dueDateBadge);

    return secondInnerSpan;
}

function addToLocalStorage(taskObj) {
    let tasks = getTasksFromLocalStorage();
    tasks.push(taskObj);
    localStorage.setItem(TASKS_KEY, JSON.stringify(tasks));
}

function clearAllTasks() {
    // clear all
    while(taskList.firstChild) {
        taskList.firstChild.remove();
    }
}


function addFilteredTasks(filteredTasks) {
    filteredTasks.forEach(function(task) {
        addTaskElement(task);
    });
}

function showFilteredTasks(tasks, priority) {
    const filteredTasks = tasks.filter(function(taskObj) {
        return ((taskObj.priority) === priority);
    });
    // show filtered tasks
    addFilteredTasks(filteredTasks);
    return filteredTasks;
}

function lowToHighSort() {
    let updatedTasksList = [];
    const tasks = filteredTasksList;
    clearAllTasks();
    updatedTasksList.push(...showFilteredTasks(tasks, 'Low'));
    updatedTasksList.push(...showFilteredTasks(tasks, 'Med'));
    updatedTasksList.push(...showFilteredTasks(tasks, 'High'));
    filteredTasksList = updatedTasksList;
}

function lowToHighFilter() {
    syncWithSearch();
    syncWithDueDateFilters(filteredTasksList);
    lowToHighSort();
}

function highToLowSort() {
    let updatedTasksList = [];
    const tasks = filteredTasksList;
    clearAllTasks();
    updatedTasksList.push(...showFilteredTasks(tasks, 'High'));
    updatedTasksList.push(...showFilteredTasks(tasks, 'Med'));
    updatedTasksList.push(...showFilteredTasks(tasks, 'Low'));
    filteredTasksList = updatedTasksList;
}

function highToLowFilter() {
    syncWithSearch();
    syncWithDueDateFilters(filteredTasksList);
    highToLowSort();
}


function defaultPriorityFilter() {
    const tasks = getTasksFromLocalStorage();
    clearAllTasks();
    addFilteredTasks(tasks);
    filteredTasksList = tasks;
    if(dueDateFilter.value != 'Default') 
        handleDueDateFilters();
}

function syncWithDueDateFilters(tasks) {
    if(dueDateFilter.value === 'Asc to Desc')
        filteredTasksList = AscToDescDateSort();

    else if(dueDateFilter.value === 'Desc to Asc')
        filteredTasksList = DesctoAscDateSort();
    else {
        clearAllTasks();
        addFilteredTasks(tasks);
    }
}
function syncWithPriorityFilters(sortedTasks) {
    if(priorityFilter.value === 'Low to High')
        lowToHighSort();
    else if(priorityFilter.value === 'High to Low')
        highToLowSort();
    else{
        clearAllTasks();
        addFilteredTasks(sortedTasks);
    }
}

function AscToDescDateSort() {
    const tasksToSort = filteredTasksList;
    quickSort(tasksToSort, 0, tasksToSort.length - 1);
    filteredTasksList = tasksToSort;
    clearAllTasks();
    return tasksToSort;
}

function DesctoAscDateSort() {
    const tasksToSort = filteredTasksList;
    quickSort(tasksToSort, 0, tasksToSort.length - 1);
    filteredTasksList = [];
    for(let i = tasksToSort.length - 1; i >= 0; i--) {
        filteredTasksList.push(tasksToSort[i]);
    }
    clearAllTasks();
    return filteredTasksList;
}

function AscToDescDateFilter() {
    // filteredTasksList = getTasksFromLocalStorage();
    syncWithSearch();
    let tasks = AscToDescDateSort();
    clearAllTasks();
    syncWithPriorityFilters(tasks);
}

function DesctoAscDateFilter() {
    // filteredTasksList = getTasksFromLocalStorage();
    syncWithSearch();
    let tasks = DesctoAscDateSort();
    clearAllTasks();
    syncWithPriorityFilters(tasks);
}

function defaultDueDateFilter() {
    syncWithSearch();
    clearAllTasks();
    let tasks = filteredTasksList;
    syncWithPriorityFilters(tasks);
}

// Event Handlers

function handleClearAll(e) {
    e.preventDefault();
    clearAllTasks();
    localStorage.setItem(TASKS_KEY, JSON.stringify([]));
}

function handleRemove(e) {
    e.preventDefault();
    if(isDeleteElement(e)) {
        let taskListItem = e.target.parentElement;
        while(!(taskListItem.classList.contains('task-item'))) {
            taskListItem = taskListItem.parentElement;
        }
        const taskIdToBeDeleted = parseInt(taskListItem.dataset.id);
        taskListItem.remove();
        let localTasks = getTasksFromLocalStorage();
        const updatedTasks = localTasks.filter(function(task) {return task.id !== taskIdToBeDeleted; })
        localStorage.setItem(TASKS_KEY, JSON.stringify(updatedTasks));  
    }
}

function handleSubmit(e) {
    e.preventDefault();
    
    const task = taskElement.value;
    const dueDate = dueDatePicker.value;
    const priority = prioritySelect.value;

    if(task === '' || dueDate === '' || priority === '' ) {
        emptyModalObj.open();
        return;
    }
    const taskObj = {id: new Date().getTime(), task: task, dueDate, priority}
    addTaskElement(taskObj);
    addToLocalStorage(taskObj);
    taskElement.value = '';
}

function loadTasks() {
    let tasks = getTasksFromLocalStorage();
    tasks.forEach(function(taskObj) {
        addTaskElement(taskObj);
    });
}

function syncWithSearch() {
    filteredTasksList = getTasksFromLocalStorage();
    if(taskSearchElement.value === '') {
        return;
    }
    else {

        const filteredTasks = filteredTasksList.filter(function(taskObj) {
            return taskObj.task.toLowerCase().indexOf((taskSearchElement.value).toLowerCase()) !== -1;
        });
        filteredTasksList = filteredTasks;
        // // clear all
        // clearAllTasks()
        // // show filtered tasks
        // filteredTasksList.forEach(function(task) {
        //     addTaskElement(task);
        // });
    }
}


function handleSearch() {
    filteredTasksList = getTasksFromLocalStorage();

    const filteredTasks = filteredTasksList.filter(function(taskObj) {
        return taskObj.task.toLowerCase().indexOf((taskSearchElement.value).toLowerCase()) !== -1;
    });
    filteredTasksList = filteredTasks;
    syncWithDueDateFilters(filteredTasksList);
    syncWithPriorityFilters(filteredTasksList);
    // clear all
    clearAllTasks()
    // show filtered tasks
    filteredTasksList.forEach(function(task) {
        addTaskElement(task);
    });
}

function handleRemoveTask(e) {
    let taskIdToBeDeleted = parseInt(e.target.dataset.taskidtobedeleted);
    let taskListItem;
    for(let i =0; i < tasks.length; i++) {
        if(parseInt(tasks[i].dataset.id) === taskIdToBeDeleted) {
            taskListItem = tasks[i];
            break;
        }
    }
    taskListItem.remove();
    let localTasks = getTasksFromLocalStorage();
    const updatedTasks = localTasks.filter(function(task) {return task.id !== taskIdToBeDeleted; })
    localStorage.setItem(TASKS_KEY, JSON.stringify(updatedTasks));
}

function handleConfirm(e) {
    e.preventDefault();
    if(isDeleteElement(e)) {
        let taskListItem = e.target.parentElement;
        while(!(taskListItem.classList.contains('task-item'))) {
            taskListItem = taskListItem.parentElement;
        }
        const taskIdToBeDeleted = parseInt(taskListItem.dataset.id);
        removeTask.setAttribute('data-taskIdToBeDeleted', taskIdToBeDeleted);
        removeModalObj.open();   
    }
}

function handlePriorityFilters(e) {
    const selectedFilter = priorityFilter.value;
   
    if(selectedFilter == 'Low to High')
        lowToHighFilter();
    else if(selectedFilter == 'High to Low')
        highToLowFilter();
    else 
        defaultPriorityFilter();
}

function handleDueDateFilters(e) {
    const selectedFilter = dueDateFilter.value;

    if(selectedFilter == 'Asc to Desc') 
        AscToDescDateFilter();
    else if(selectedFilter == 'Desc to Asc') 
        DesctoAscDateFilter();
    else 
        defaultDueDateFilter();

}

function handleEditSubmit(e) {
    e.preventDefault();
    let taskIdToBeEdited = parseInt(e.target.dataset.taskidtobeedited);
    let taskListItem;
    for(let i =0; i < tasks.length; i++) {
        if(parseInt(tasks[i].dataset.id) === taskIdToBeEdited) {
            taskListItem = tasks[i];
            break;
        }
    }
    const task = editTask.value;
    const priority = editPrioritySelect.value;
    const dueDate = editDueDatePicker.value;

    if(task === '' || dueDate === '' || priority === '' ) {
        emptyModalObj.open();
        return;
    }
    const taskObj = {id: taskIdToBeEdited, task: task, dueDate, priority}

    taskListItem.childNodes[0].childNodes[0].nodeValue = `${task}`;
    badgeElement = taskListItem.childNodes[1];
    badgeElement.replaceWith(addBadges(taskObj));
    let localTasks = getTasksFromLocalStorage();
    localTasks.forEach(function(task, index){
        if(parseInt(task.id) === taskIdToBeEdited) {
            localTasks[index] = taskObj;
        }
    })

    localStorage.setItem(TASKS_KEY, JSON.stringify(localTasks));
    editModalObj.close();
}


function handleEditForm(e) {
    if(isEditElement(e)) {
        let taskListItem = e.target.parentElement;
        while(!(taskListItem.classList.contains('task-item'))) {
            taskListItem = taskListItem.parentElement;
        }
        const taskIdToBeEdited = parseInt(taskListItem.dataset.id);
        editTaskSubmit.setAttribute('data-taskIdToBeEdited', taskIdToBeEdited)
        editModalObj.open();
    }
}

function initializeModals() {
    emptyModalObj = M.Modal.init(emptyModal);
    removeModalObj = M.Modal.init(removeModal);
    editModalObj = M.Modal.init(editModal);
    nameModalObj = M.Modal.init(nameModal);
    dueDateObj = M.Datepicker.init(dueDatePicker);
    editDueDateObj = M.Datepicker.init(editDueDatePicker);
    prioritySelectObj = M.FormSelect.init(prioritySelect);
    editPrioritySelectObj = M.FormSelect.init(editPrioritySelect);
    priorityFilterObj = M.FormSelect.init(priorityFilter);
    dueDateFilterObj = M.FormSelect.init(dueDateFilter);
}


// Sorting Functions
function getMedian(x, low, middle, high)
{
    let lowDueDate = Date.parse(x[low].dueDate);
    let middleDueDate = Date.parse(x[middle].dueDate);
    let highDueDate = Date.parse(x[high].dueDate);

    if((lowDueDate < middleDueDate && middleDueDate < highDueDate) || 
    (lowDueDate > middleDueDate && middleDueDate > highDueDate))
        return middle;
    
    if((middleDueDate < lowDueDate && lowDueDate < highDueDate) ||
    (middleDueDate > lowDueDate) && lowDueDate > highDueDate)
        return low;

    return high;
}

function partition(x, lb, ub)
{
   let down, up, pivot, median;
    down = lb;
    up = ub;
    median = getMedian(x, lb, Math.floor((lb+ub)/2), ub);
    // swap(x[lb], x[median]);
    [x[lb], x[median]] = [x[median], x[lb]];

    pivot = Date.parse(x[lb].dueDate);
    while(down < up)
    {
        while(Date.parse(x[down].dueDate) <= pivot && down < ub)
            down++;
        
        while(Date.parse(x[up].dueDate) > pivot)
            up--;

        if(down < up)
            [x[down], x[up]] = [x[up], x[down]] ;
    }
    // swap(x[lb], x[up]);
    [x[lb], x[up]] = [x[up], x[lb]];

    return up;
}

function quickSort(x, lb, ub) {
    let pivotIndex;
    if(lb < ub)
    {
        pivotIndex = partition(x, lb, ub);
        quickSort(x, lb, pivotIndex-1);
        quickSort(x, pivotIndex+1, ub);
    }
}


// Greetings
function handleGreeting() {
    // Name of user
    if(localStorage.getItem('name')!=null) {
        userName = localStorage.getItem('name');
        greeting();
    }
    else {
        nameModalObj.open();
        greeting();
    }
}

function getName(e) {
    userName = inputUserName.value;
    localStorage.setItem('name',userName);
}

function greeting() {
    // Time of day
    const hours = (new Date()).getHours();
    let time;
    if(hours > 0 && hours < 12) {
    time = 'Morning';
    } else if(hours >= 12 && hours <= 16) {
    time = 'Afternoon';
    } else {
    time = 'Evening';
    }

    const span = document.createElement('span');
    span.className = "greet grey-text text-darken-3";
    text = document.createTextNode(`Good ${time} ${userName}☕️!`);
    span.appendChild(text);

    greetings = document.querySelector('.greetings');
    greetings.appendChild(span);
}