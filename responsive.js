if(window.innerWidth > 600 && window.innerWidth < 800) {
    m3Classes = document.querySelector('.taskListSection').querySelectorAll('.m3');
    m6Classes = document.querySelector('.taskListSection').querySelectorAll('.m6');
    
    m3Classes.forEach(function(item){
        item.classList.remove('m3');
        item.classList.add('m6');
    });
    
    m6Classes.forEach(function(item){
        item.classList.remove('m6');
        item.classList.add('m12');
    });
}